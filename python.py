import matplotlib.pyplot as plt
from datetime import datetime
from matplotlib import pyplot
from matplotlib.animation import FuncAnimation
from random import randrange

#estilos al grafico
plt.style.use('ggplot')

#listas en X Y
x_data = []
y_data = []

#CREA LA FIGURA
figure = pyplot.figure()

line, = pyplot.plot_date
(x_data, y_data, '-')

#FUNCION DONDE GENERA LOS VALORES ALEATORIOS
def grafica3 (frame):
    x_data.append(datetime.now())
    y_data.append(randrange(0,100))
    line.set_data(x_data, y_data)
    figure.gca().relim()
    figure.gca().autoscale_view()
    #DEVUELVE LA INFORMACION DE LA POSICION DEL GRAFICO
    return line,
    
#LIBRERIAS PARA MOSTRAR DATOS EN TIEMPO REAL
animacion3 = FuncAnimation(figure, grafica3, interval=300)
pyplot.show()
    
    
